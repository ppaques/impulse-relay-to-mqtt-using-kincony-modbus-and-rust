#!/bin/bash

# first args is the sleep time in second
SLEEP_TIME=$1
KINCONY_TOPIC="relay32/4ab843d38a5a21ae54def363/set"
PAYLOAD_SET="{\"relay15\":{\"on\":1}}"
PAYLOAD_RESET="{\"relay15\":{\"on\":0}}"
BROKER="192.168.178.111"

#note: you can follow messages with
mosquitto_sub -v -t "relay32/#" -h 192.168.178.111 -F "%I %r %t %p" &

while [ 1 ]
do 
    echo "Publish SET"
    mosquitto_pub -h $BROKER -t $KINCONY_TOPIC -m $PAYLOAD_SET
    sleep $SLEEP_TIME
    echo "Publish RESET"
    mosquitto_pub -h $BROKER -t $KINCONY_TOPIC -m $PAYLOAD_RESET
    sleep $SLEEP_TIME
done