use clap::Parser;
use modbus::Client;
use modbus::tcp;
use std::{thread, time, sync::mpsc};
use rumqttc::{MqttOptions, QoS};
use std::str;
use serde_json::Value;


/// Mqtt switch emulator for Kincony H32B
///   - Read status from modbus device
///   - Write command to MQTT
/// 
/// The Kincony H32b should be set with impulse mode
///
/// Inputs:
///   - Modbus 32 bits of status
///   - Mqtt:
///     - 6 bits of status
///     - 32 relays states
/// Outputs:
///   - Mqtt: 32 relays states     
///
/// For lights, let's do something simple:
///    light/x/state        1|0     -> get state
///    light/x/state/set    1|0     -> set desired state
///    light/x/state/toggle 1       -> revert actual state
/// 
/// For inputs:
///    input/x/state        1|0
/// 
/// For Coil set
///     relay/x/state       1|0
///     relay/x/state/set   1|0
///     relay/x/state/toggle 1 


#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// hostname of TCP modbus status device 
    #[clap(short, long, default_value_t=String::from("192.168.178.210"))]
    hostname: String,

    /// hostname of Mqtt 
    #[clap(short, long, default_value_t=String::from("192.168.178.111"))]
    broker: String,

    // kincony topic
    #[clap(short, long, default_value_t=String::from("relay32/4ab843d38a5a21ae54def363"))]
    kincony: String,

    // light amount of light starting from bit0
    #[clap(short, long, default_value_t=24)]
    light: u16,

    // all others are mapped as relays

    // Port of TCP modbus status device
    #[clap(short, long, default_value_t=8080)]
    port: u16,

    /// activate verbose messages
    #[clap(short, long)]
    verbose: bool

}


enum RequestType {
    Toggle,
    Set,
    Reset
}

struct CommandRequest{
    id: i8,
    request: RequestType
}


fn main() {
    let args = Args::parse();
    if args.verbose {
        println!("Trying to connect to {:?}:{:?}", args.hostname, args.port)
    }

    let mut cfg = tcp::Config::default();
    cfg.tcp_port = args.port;
    let mut modbus_client = tcp::Transport::new_with_cfg(&args.hostname, cfg).unwrap();

    let (status_tx, status_rx) = mpsc::channel();

    if args.verbose {
        println!("Staring modbus-reader thread");
    }
    // modbus pooler
    thread::Builder::new().name("modbus-reader".to_string()).spawn(move || {
        // some work here
        loop{
            let res = modbus_client.read_holding_registers(4, 2).unwrap();
            let latest_read = (*res.get(0).unwrap() as u32) | (*res.get(1).unwrap() as u32)<<16;
            status_tx.send(latest_read).unwrap();
            thread::sleep(time::Duration::from_millis(100));
        }
    }).unwrap();


    // MQTT connection creation
    let i: i32 = rand::random();
    let identity = format!("telerupt-{}", i);
    if args.verbose {
        println!("connect mqtt to {} with identiy {}", args.broker, identity);
    }
    let mut mqttoptions = MqttOptions::new(identity, args.broker, 1883);
    mqttoptions.set_keep_alive(time::Duration::from_secs(5));
    let (mut client, mut connection) = rumqttc::Client::new(mqttoptions, 10);

    // start Mqtt subscriptions
    let topic_kincony_state = format!("{}/state", args.kincony);
    let topic_kincony_state_2 = topic_kincony_state.clone();
    let topic_kincony_set = format!("{}/set", args.kincony);
    if args.verbose {
        println!("Mqtt subscribe to {}", topic_kincony_state);
    }
    client.subscribe(topic_kincony_state, QoS::AtMostOnce).unwrap();
    
    let topic_light_set = "light/+/state/set";
    let topic_light_toggle = "light/+/state/toggle";
    if args.verbose {
        println!("Mqtt subscribe to {}", topic_light_set);
        println!("Mqtt subscribe to {}", topic_light_toggle);
    }
    client.subscribe(topic_light_set, QoS::AtMostOnce).unwrap();
    client.subscribe(topic_light_toggle, QoS::AtMostOnce).unwrap();


    let topic_relay_set = "relay/+/state/set";
    let topic_relay_toggle = "relay/+/state/toggle";
    if args.verbose {
        println!("Mqtt subscribe to {}", topic_relay_set);
        println!("Mqtt subscribe to {}", topic_relay_toggle);
    }
    client.subscribe(topic_relay_set, QoS::AtMostOnce).unwrap();
    client.subscribe(topic_relay_toggle, QoS::AtMostOnce).unwrap();


    // data information from mqtt thread to main
    //   - states first 32 bits set to 1 
    let (relay_states_tx, relay_states_rx) = mpsc::channel();
    let (request_tx, request_rx) = mpsc::channel();

    // create a thread for iterating onto the event loop of mqtt
    // this thread is responsibletopic_light_toggletopic_light_toggle to send a receive messages
    thread::Builder::new().name("mqtt-thread".to_string()).spawn(move || {
        // Iterate to poll the eventloop for connection progress
        loop {
            for notification in connection.iter() {
                match notification {
                    Ok(rumqttc::Event::Incoming(rumqttc::Packet::Publish(msg))) => {
                        let topic = msg.topic;
                        // Manage and update all cases
                        if topic == topic_kincony_state_2 {
                            let utf8_payload = str::from_utf8(&msg.payload);
                            match utf8_payload {
                                Ok(utf8) => {
                                    match serde_json::from_str::<Value>(utf8) {
                                        Ok(json) => {
                                            // create new relay states
                                            // Object({"input1": Object({"on": Number(0)}), "input2": Object({"on": Number(0)}), "input3": Object({"on": Number(0)}), "input4": Object({"on": Number(0)}), "input5": Object({"on": Number(0)}), "input6": Object({"on": Number(0)}), "relay1": Object({"on": Number(0)}), "relay10": Object({"on": Number(0)}), "relay11": Object({"on": Number(0)}), "relay12": Object({"on": Number(0)}), "relay13": Object({"on": Number(0)}), "relay14": Object({"on": Number(0)}), "relay15": Object({"on": Number(0)}), "relay16": Object({"on": Number(0)}), "relay17": Object({"on": Number(0)}), "relay18": Object({"on": Number(0)}), "relay19": Object({"on": Number(0)}), "relay2": Object({"on": Number(0)}), "relay20": Object({"on": Number(0)}), "relay21": Object({"on": Number(0)}), "relay22": Object({"on": Number(0)}), "relay23": Object({"on": Number(0)}), "relay24": Object({"on": Number(0)}), "relay25": Object({"on": Number(0)}), "relay26": Object({"on": Number(0)}), "relay27": Object({"on": Number(0)}), "relay28": Object({"on": Number(0)}), "relay29": Object({"on": Number(0)}), "relay3": Object({"on": Number(0)}), "relay30": Object({"on": Number(0)}), "relay31": Object({"on": Number(0)}), "relay32": Object({"on": Number(0)}), "relay4": Object({"on": Number(0)}), "relay5": Object({"on": Number(0)}), "relay6": Object({"on": Number(0)}), "relay7": Object({"on": Number(0)}), "relay8": Object({"on": Number(0)}), "relay9": Object({"on": Number(0)})})
                                            // # This can be also partial object like: {"relay15":{"on":1}}
                                            let mut relay_states :u64 =0;
                                            for n in 1..33 {
                                                let relay_name = format!("relay{}", n);
                                                match json[relay_name]["on"].as_u64() {
                                                    Some(on) => { relay_states |= on << n-1; },
                                                                _ => {}
                                                            }
                                                }
                                            relay_states_tx.send(relay_states).unwrap();
                                        },
                                        Err(e) => {
                                            print!("Cannot convert kincony json {}: {:?} ", e, msg.payload)
                                        }
                                    }

                                },
                                Err(e) => {
                                    print!("Cannot convert kincony {}: {:?} ", e, msg.payload)
                                }
                            }

                        } else {
                            // manage request
                            let topic_split : Vec<&str> = topic.split("/").collect();
                            // We want to manage:
                            //  - light/x/state/set  "1|0"
                            //  - light/x/state/toggle  "1"
                            //  - relay/x/state/set   "1|0"
                            //  - relay/x/state/toggle
                            if  (topic_split.len() == 4) && 
                                (topic_split[0] == "light" || topic_split[0] == "relay") &&
                                (topic_split[2] == "state") &&
                                (topic_split[3] == "set" || topic_split[3] == "toggle" ) {
                                    // parse the number or fail
                                    let i = match topic_split[1].parse::<i8>() {
                                        Ok(i) => i,
                                        Err(_e) => -1,
                                    };
                                    if i > 0 && i < 33 {
                                        let active = msg.payload == "1";
                                        let request_type : RequestType;
                                        if topic_split[3] == "set" {
                                            if active {request_type = RequestType::Set;}
                                            else { request_type = RequestType::Reset;}
                                        } else {
                                            request_type = RequestType::Toggle;
                                        }
                                        request_tx.send(CommandRequest{ id: i, request: request_type}).unwrap();

                                    } else {
                                        print!("unamanged topic {}", topic); 
                                    }


                            } else {
                                print!("unamanged topic {}", topic);
                            }
                        }

                    }
                    _ => {}//println!("Ignoring notification {:?}", notification)
                }
            }
        }
    }).unwrap();

    // prepare topics
    client.publish("telerupt/alive", QoS::AtLeastOnce, true, "1").unwrap();

    // publish 0 for all lights
    for l in 1..(args.light+1) {
       client.publish(format!("light/{}/state",l), QoS::AtLeastOnce, true, "0").unwrap(); 
    }
    // publish 0 for all states
    for s in (args.light+1)..33 {
        client.publish(format!("input/{}/state",s), QoS::AtLeastOnce, true, "0").unwrap();        
    }
    // publish 0 for all relays
    for r in (args.light+1)..33 {
        client.publish(format!("relay/{}/state",r), QoS::AtLeastOnce, true, "0").unwrap();
    }
    
    let mut actual_status = 0;
    let mut actual_relay_status = 0;
    loop {
        match status_rx.try_recv(){
            Ok(received_status) => {
                if actual_status != received_status {
                    if args.verbose {
                        println!("State changed {:#032b} -> {:#032b}", actual_status, received_status);
                    }

                    // detect change only over the value
                    let diff = actual_status ^ received_status;
                    let mut mask = 1u32; // assuming rightmost bit first
                    for i in 1..33 {
                        let is_changed = diff & mask != 0;
                        if is_changed{
                            let new_val = if received_status&mask != 0 {"1"} else {"0"};
                            if i < args.light+1 {
                                if args.verbose {
                                    println!("Light {} changed {:?} -> {:?}", i,  (actual_status&mask)>>i-1, (received_status&mask)>>i-1);
                                }
                                client.publish(format!("light/{}/state", i), QoS::AtLeastOnce, true, new_val).unwrap();
                                
                            } else {
                                if args.verbose {
                                    println!("State {} changed {:?} -> {:?}", i,  (actual_status&mask)>>i-1, (received_status&mask)>>i-1);
                                }
                                client.publish(format!("input/{}/state", i), QoS::AtLeastOnce, true, new_val).unwrap();
                            }
                        }


                        mask <<= 1; // assuming rightmost bit first
                    }


                    actual_status = received_status;
                }
            },
            Err(mpsc::TryRecvError::Empty) => { }
            Err(mpsc::TryRecvError::Disconnected) => { panic!("Relay RX signal disconnected") }
        }

        match relay_states_rx.try_recv(){
            Ok(received_relay_status) => {
                if actual_relay_status != received_relay_status {
                    if args.verbose {
                        println!("Relays changed {:#032b} -> {:#032b}", actual_relay_status, received_relay_status);


                    // detect change only over the value
                    let diff = actual_relay_status ^ received_relay_status;
                    let mut mask = 1u64; // assuming rightmost bit first
                    for i in 1..33 {
                        let is_changed = diff & mask != 0;
                        if is_changed {
                            let new_val_bool = received_relay_status&mask != 0;
                            let new_val_str = if new_val_bool {"1"} else {"0"};
                            if i < args.light+1 {
                                if args.verbose {
                                    println!("Telerupt {} is switching {:?} -> {:?}", i,  (actual_relay_status&mask)>>i-1, (received_relay_status&mask)>>i-1);
                                }
                                // Stop the coil switch ASAP (prevent 1 second pulse)
                                //if new_val_bool {
                                //    client.publish(topic_kincony_set.clone(), QoS::AtLeastOnce, true, format!("{{\"relay{}\":{{\"on\":0}}}}", i)).unwrap();
                                //}
                                
                            } else {
                                if args.verbose {
                                    println!("Relay {} changed {:?} -> {:?}", i,  (actual_relay_status&mask)>>i-1, (received_relay_status&mask)>>i-1);
                                }
                                client.publish(format!("relay/{}/state", i), QoS::AtLeastOnce, true, new_val_str).unwrap();
                            }
                        }


                        mask <<= 1; // assuming rightmost bit first
                    }


                    }
                    actual_relay_status = received_relay_status;
                }
            },
            Err(mpsc::TryRecvError::Empty) => { }
            Err(mpsc::TryRecvError::Disconnected) => { panic!("Relay RX signal disconnected") }

        }

        match request_rx.try_recv(){
            Ok(new_request) => {
                let actual_state = if new_request.id <= args.light as i8 {
                        actual_status & (1<<(new_request.id -1)) != 0
                    } else {
                        // we are a relay
                        actual_relay_status & (1<<(new_request.id -1)) != 0
                    };
                match new_request.request {
                    
                    RequestType::Toggle => {
                        let on_off = if !actual_state || new_request.id <= args.light as i8 {"1"} else {"0"};
                        client.publish(topic_kincony_set.clone(), QoS::AtLeastOnce, false, format!("{{\"relay{}\":{{\"on\":{}}}}}", new_request.id, on_off )).unwrap();
                        if args.verbose {
                            println!("Toggle {} changed {:?} -> {:?}", new_request.id,  actual_state, on_off);
                        }                      
                    },
                    RequestType::Set => {
                        if !actual_state  {
                            client.publish(topic_kincony_set.clone(), QoS::AtLeastOnce, false, format!("{{\"relay{}\":{{\"on\":1}}}}", new_request.id)).unwrap();
                            if args.verbose {
                                println!("Set {}", new_request.id);
                            }
                        } else {
                            if args.verbose {
                                println!("Ignore Set {}", new_request.id);
                            }
                        }
                        
                    },
                    RequestType::Reset => {
                        if actual_state  {
                            // for light we pulse
                            if new_request.id <= args.light as i8 {
                                client.publish(topic_kincony_set.clone(), QoS::AtLeastOnce, false, format!("{{\"relay{}\":{{\"on\":1}}}}", new_request.id)).unwrap();
                            } else {
                                client.publish(topic_kincony_set.clone(), QoS::AtLeastOnce, false, format!("{{\"relay{}\":{{\"on\":0}}}}", new_request.id)).unwrap();
                            }
                            if args.verbose {
                                println!("Reset {}", new_request.id);
                            }
                        } else {
                            if args.verbose {
                                println!("Ignore Reset {}", new_request.id);
                            }
                        }  
                    }
                }
            },
            Err(mpsc::TryRecvError::Empty) => { }
            Err(mpsc::TryRecvError::Disconnected) => { panic!("request RX signal disconnected") }

        }
        
        //client.publish("hello/rumqtt", QoS::AtMostOnce, false, vec![i; i as usize]).unwrap();
        thread::sleep(time::Duration::from_millis(50));
    }
    // todo: handle gracefully control-c

}
